var express = require('express');
var app = express();
var path = require('path');
var publicPath = path.join(__dirname + '/public/');
var mysqldb = require("./mysqldb");

var tableName = 'ecu_information';

app.get('/', function(request, response) {
    response.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/public', express.static(publicPath))

app.get('/data', function(request, response) {
    var queryString = 'SELECT * FROM ' + tableName;
    mysqldb.query(queryString, function(err, rows, fields) {
        if (err) {
            console.log(err);
            throw err;
        } else {
            response.send(rows);
        }
    });
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  host = (host === '::' ? 'localhost' : host);
  var port = server.address().port;
 
  console.log('Running server at http://%s:%s', host, port);
});
