setInterval(function() {
    $(function () {
        $.ajax({
            type: 'GET',
            url: '/data',
            success: function(result) {
                document.getElementById('voltage').innerHTML = "Voltage:  " + result[0].voltage;
                document.getElementById('current').innerHTML = "Current:  " + result[0].current;
                document.getElementById('out-wind').innerHTML = "Outlet Wind Speed:  " + result[0].indoor_temp;
                document.getElementById('out-temp').innerHTML = "Outlet Temp:  " + result[0].outlet_temp;
                document.getElementById('indoor-temp').innerHTML = "Indoor Temp:  " + result[0].outlet_wind_speed;
                document.getElementById('message').innerHTML = result[0].message;
            }
        });
    });
}, 5 * 1000);